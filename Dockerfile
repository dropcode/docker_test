FROM openjdk:8
ADD target/test-application.jar test-application.jar
EXPOSE 4040
ENTRYPOINT ["java", "-jar", "test-application.jar"]